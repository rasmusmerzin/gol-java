# Game of Life

Simple [John Conway](https://en.wikipedia.org/wiki/John_Horton_Conway)'s [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) implementation in Java.

<img src="./demo.gif" alt="demo.gif" style='width:100%' />

## Setup

[Gradle](https://gradle.org) 6.6.1 & [OpenJDK](https://openjdk.java.net) 14.0.2 were used while developing this project.
Terminal IO is handled with [JLine](https://github.com/jline) 3.16.0.

Cloning this repository `git clone https://gitlab.com/rasmusmerzin/gol-java.git`

Changing working directory `cd gol-java`

Building the jar file `gradle jar`

Running the jar file `java -jar build/libs/gol.jar`

## Keybinds

| KeyBind           | Description                     |
| ----------------- | ------------------------------- |
| h,j,k,l \/ Arrows | Move cursor left,down,up,right  |
| 0                 | Move cursor to the first column |
| \$                | Move cursor to the last column  |
| g                 | Move cursor to the first line   |
| G                 | Move cursor to the last line    |
| z                 | Move cursor to the center       |
| i \/ a            | Make cell alive                 |
| x \/ d            | Make cell dead                  |
| Space             | Toggle cell                     |
| Ctrl + l          | Clean world                     |
| Ctrl + n          | Next iteration                  |
| Ctrl + p          | Previous iteration              |
| Enter             | Start \/ stop simulation        |
