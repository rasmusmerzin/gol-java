package gol;

import java.io.PrintWriter;
import java.lang.InterruptedException;
import java.lang.Thread;

public class WorldIterator extends Thread {
  private final World world;

  public WorldIterator(World world) { this.world = world; }

  public void run() {
    world.hideCursor();

    while (true) {
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        break;
      }

      if (!world.iterate()) {
        world.showCursor();
        world.renderStatus("stopped");
        break;
      }
    }
  }
}
