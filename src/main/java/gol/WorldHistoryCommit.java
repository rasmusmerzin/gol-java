package gol;

public class WorldHistoryCommit {
  public final int row;
  public final int col;
  public final boolean state;

  public WorldHistoryCommit(int row, int col, boolean state) {
    this.row = row;
    this.col = col;
    this.state = state;
  }
}
