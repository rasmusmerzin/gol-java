package gol;

import java.io.IOException;
import java.io.PrintWriter;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.utils.NonBlockingReader;

public class Game {
  private final Terminal terminal;
  private final NonBlockingReader reader;
  private final PrintWriter writer;
  private final World world;
  private WorldIterator worldIterator;

  private int amount = 0;

  public Game() throws IOException {
    terminal = TerminalBuilder.builder().system(true).build();

    reader = terminal.reader();
    writer = terminal.writer();

    world =
        new World(writer, terminal.getHeight() - 1, terminal.getWidth() / 2);
    worldIterator = new WorldIterator(world);
  }

  private void nextAction(int key) {
    world.renderStatus("");

    if (worldIterator.isAlive()) {
      worldIterator.interrupt();
      world.showCursor();
      world.renderStatus("paused");
      if (key == 13)
        return;
    }

    if (key > 48 && key <= 57 || key == 48 && amount > 0) {
      amount = amount * 10 + key - 48;
      world.renderStatus("" + amount);
    } else {
      switch (key) {
      case 68:
      case 104:
        world.moveCursor(Direction.LEFT, amount > 0 ? amount : 1);
        break;
      case 66:
      case 106:
        world.moveCursor(Direction.DOWN, amount > 0 ? amount : 1);
        break;
      case 65:
      case 107:
        world.moveCursor(Direction.UP, amount > 0 ? amount : 1);
        break;
      case 67:
      case 108:
        world.moveCursor(Direction.RIGHT, amount > 0 ? amount : 1);
        break;

      case 36:
        world.moveCursor(world.getCursorRow(), world.getWidth() - 1);
        break;
      case 48:
        world.moveCursor(world.getCursorRow(), 0);
        break;
      case 103:
        world.moveCursor(0, world.getCursorCol());
        break;
      case 71:
        world.moveCursor(world.getHeight() - 1, world.getCursorCol());
        break;
      case 122:
        world.moveCursor(world.getHeight() / 2, world.getWidth() / 2);
        break;

      case 97:
      case 105:
        world.set(true);
        break;
      case 100:
      case 120:
        world.set(false);
        break;
      case 32:
        world.toggle();
        break;
      case 12:
        world.clear();
        world.renderStatus("cleared");
        break;
      case 13:
        worldIterator = new WorldIterator(world);
        worldIterator.start();
        world.renderStatus("running");
        break;
      case 14:
        for (int i = 0; i < (amount > 0 ? amount : 1); i++) {
          if (world.iterate())
            world.renderStatus("iterate");
          else {
            world.renderStatus("no change");
            break;
          }
        }
        break;
      case 16:
        for (int i = 0; i < (amount > 0 ? amount : 1); i++) {
          if (world.rollback())
            world.renderStatus("rollback");
          else {
            world.renderStatus("start of history reached");
            break;
          }
        }
        break;

        // default: world.renderStatus("\033[2mKEY" + key + "\033[m");
      }

      amount = 0;
    }
  }

  public void launch() throws IOException {
    terminal.enterRawMode();

    new Welcome(writer, terminal.getHeight(), terminal.getWidth()).render();
    reader.read();
    world.render();

    while (true)
      nextAction(reader.read());
  }
}
