package gol;

import java.io.PrintWriter;
import java.util.ArrayList;

public class World {
  private boolean loop = false;
  private int height;
  private int width;
  private int cursorRow;
  private int cursorCol;
  private boolean[][] cells;
  private boolean cursorVisible = true;
  private final WorldHistory history = new WorldHistory();
  private final PrintWriter writer;

  public World(PrintWriter writer, int rows, int cols) {
    this.writer = writer;
    this.height = rows;
    this.width = cols;
    cursorRow = rows / 2;
    cursorCol = cols / 2;
    cells = new boolean[rows][cols];
  }

  public int getWidth() { return width; }

  public int getHeight() { return height; }

  public int getCursorRow() { return cursorRow; }

  public int getCursorCol() { return cursorCol; }

  public void clear() {
    cells = new boolean[height][width];
    history.clear();
    render();
  }

  public void hideCursor() {
    cursorVisible = false;
    renderCursor();
  }

  public void showCursor() {
    cursorVisible = true;
    renderCursor();
  }

  public void moveCursor(Direction dir, int amount) {
    hideCursor();
    switch (dir) {
    case LEFT:
      cursorCol = Math.max(cursorCol - amount, 0);
      break;
    case DOWN:
      cursorRow = Math.min(cursorRow + amount, height - 1);
      break;
    case UP:
      cursorRow = Math.max(cursorRow - amount, 0);
      break;
    case RIGHT:
      cursorCol = Math.min(cursorCol + amount, width - 1);
      break;
    }
    showCursor();
  }

  public void moveCursor(Direction dir) { moveCursor(dir, 1); }

  public void moveCursor(int row, int col) {
    hideCursor();
    cursorRow = Math.max(Math.min(row, height - 1), 0);
    cursorCol = Math.max(Math.min(col, width - 1), 0);
    showCursor();
  }

  public int clampRow(int row) {
    if (row < 0)
      return height + row % height;
    else
      return row % height;
  }

  public int clampCol(int col) {
    if (col < 0)
      return width + col % width;
    else
      return col % width;
  }

  public boolean get(int row, int col) {
    if (loop)
      return cells[clampRow(row)][clampCol(col)];
    else {
      if (row >= 0 && col >= 0 && row < height && col < width)
        return cells[row][col];
      else
        return false;
    }
  }

  public void set(int row, int col, boolean state) {
    cells[row][col] = state;
    renderCell(row, col, state);
    resetRenderPosition();
    writer.flush();
  }

  public void set(boolean state) { set(cursorRow, cursorCol, state); }

  public void toggle(int row, int col) { set(row, col, !get(row, col)); }

  public void toggle() { toggle(cursorRow, cursorCol); }

  private boolean getNext(int row, int col) {
    int count = 0;

    if (get(row, col - 1))
      count++;
    if (get(row, col + 1))
      count++;
    if (get(row - 1, col))
      count++;
    if (get(row + 1, col))
      count++;
    if (get(row - 1, col - 1))
      count++;
    if (get(row - 1, col + 1))
      count++;
    if (get(row + 1, col - 1))
      count++;
    if (get(row + 1, col + 1))
      count++;

    if (count == 3 || count == 2 && get(row, col))
      return true;
    else
      return false;
  }

  public boolean iterate() {
    boolean[][] newCells = new boolean[height][width];
    boolean change = false;

    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        boolean next = getNext(row, col);
        if (cells[row][col] != next) {
          if (!change) {
            change = true;
            history.iterate();
          }
          history.commit(row, col, next);
          renderCell(row, col, next);
        }
        newCells[row][col] = next;
      }
    }

    this.cells = newCells;
    resetRenderPosition();
    writer.flush();
    return change;
  }

  public boolean rollback() {
    ArrayList<WorldHistoryCommit> iteration = history.pop();
    if (iteration == null)
      return false;
    else {
      for (WorldHistoryCommit commit : iteration) {
        set(commit.row, commit.col, !commit.state);
        renderCell(commit.row, commit.col, !commit.state);
      }
      resetRenderPosition();
      writer.flush();
      return true;
    }
  }

  private void renderCell(int row, int col, boolean state) {
    String mover = String.format("\033[%d;%dH", 1 + row, 1 + col * 2);
    String cellText = "  ";
    if (cursorVisible && cursorRow == row && cursorCol == col)
      cellText = "[]";
    if (state)
      writer.print(mover + "\033[7m" + cellText + "\033[27m");
    else
      writer.print(mover + cellText);
  }

  private void resetRenderPosition() {
    writer.print("\033[" + (1 + height) + "H");
  }

  public void renderStatus(String status) {
    writer.print("\033[" + (1 + height) + "H\033[K" + status);
    writer.flush();
  }

  public void renderCursor() {
    renderCell(cursorRow, cursorCol, get(cursorRow, cursorCol));
    resetRenderPosition();
    writer.flush();
  }

  public void render() {
    writer.print("\033[H");
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        String cellText = "  ";
        if (cursorVisible && cursorRow == row && cursorCol == col)
          cellText = "[]";
        if (get(row, col))
          writer.print("\033[7m" + cellText + "\033[27m");
        else
          writer.print(cellText);
      }
      writer.println();
    }
    writer.flush();
  }
}
