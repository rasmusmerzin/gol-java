package gol;

import java.io.PrintWriter;

public class Welcome {
  private final String title = "John Horton Conway's Game of Life";
  private final String subtitle = "Implementation by Emil Rasmus Merzin";
  private final String[] controls = new String[] {
      "h,j,k,l   Move cursor",       "Arrows    Move cursor",
      "Space     Toggle cell",       "Enter     Start/stop simulation",
      "Ctrl+l    Clean world",       "Ctrl+n    Next iteration",
      "Ctrl+p    Previous iteration"};
  private final int controlsWidth;

  private final PrintWriter writer;
  private final int height;
  private final int width;

  public Welcome(PrintWriter writer, int height, int width) {
    this.writer = writer;
    this.height = height;
    this.width = width;

    int controlsWidth = 0;
    for (String keybind : controls) {
      controlsWidth = Math.max(controlsWidth, keybind.length());
    }
    this.controlsWidth = controlsWidth;
  }

  private void printCentered(String line, int width) {
    writer.println(String.format("\033[%dC%s", (this.width - width) / 2, line));
  }

  public void render() {
    writer.print("\033[2J\033[" + (height - controls.length - 3) / 2 + "H");

    printCentered("\033[1m" + title + "\033[m", title.length());
    printCentered("\033[2m" + subtitle + "\033[m", subtitle.length());

    writer.println();

    for (String keybind : controls)
      printCentered(keybind, controlsWidth);

    writer.print("\033[" + height + "H");
    writer.flush();
  }
}
