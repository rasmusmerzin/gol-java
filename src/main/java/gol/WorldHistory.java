package gol;

import java.util.ArrayList;

public class WorldHistory {
  private final ArrayList<ArrayList<WorldHistoryCommit>> history =
      new ArrayList<ArrayList<WorldHistoryCommit>>();

  public void iterate() { history.add(new ArrayList<WorldHistoryCommit>()); }

  public void clear() { history.clear(); }

  public ArrayList<WorldHistoryCommit> pop() {
    if (history.size() > 0) {
      ArrayList<WorldHistoryCommit> iteration = history.get(history.size() - 1);
      history.remove(history.size() - 1);
      return iteration;
    } else
      return null;
  }

  public void commit(int row, int col, boolean state) {
    history.get(history.size() - 1)
        .add(new WorldHistoryCommit(row, col, state));
  }
}
