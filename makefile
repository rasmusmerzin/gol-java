all: jar exe

jar:
	@gradle jar

exe:
	@echo "java -jar $$(readlink -f build/libs/gol.jar)" > gol
	@chmod +x gol

clean:
	@gradle clean

.PHONY: exe jar clean
